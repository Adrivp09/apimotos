import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-añadir-moto',
  templateUrl: './añadir-moto.page.html',
  styleUrls: ['./añadir-moto.page.scss'],
})
export class AñadirMotoPage implements OnInit {

  marca: string;
  modelo: string;
  year: string;
  precio: number;
  foto: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  addMoto() {
    this.foto = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    const stringPrecio = this.precio + "";
    const form = new FormData();

    form.append('marca',this.marca)
    form.append('modelo',this.modelo)
    form.append('year',this.year)
    form.append('foto',this.foto)
    form.append('precio', stringPrecio)
    this.router.navigate(['/home']);   
  }
}