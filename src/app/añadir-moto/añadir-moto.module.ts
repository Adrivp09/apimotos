import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AñadirMotoPageRoutingModule } from './añadir-moto-routing.module';

import { AñadirMotoPage } from './añadir-moto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AñadirMotoPageRoutingModule
  ],
  declarations: [AñadirMotoPage]
})
export class AñadirMotoPageModule {}
