import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Moto } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle-motos',
  templateUrl: './detalle-motos.page.html',
  styleUrls: ['./detalle-motos.page.scss'],
})
export class DetallePage implements OnInit {

  moto:Moto

  constructor(private data:DataService,private router:Router, private alertController: AlertController) { }

  ngOnInit() {
    this.moto = this.data.getMoto()
    if(!this.moto){
      this.router.navigate(['home'])
    }
  }

  borrar(){
    this.borrarMoto();
  }

  async borrarMoto(){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Advertencia!',
      message: 'Estás seguro de eliminar esta moto?',
      buttons: [{
          text: 'NO',
          role: 'cancel',
          cssClass: 'secondary',
        },{
          text: 'SI',
          handler: () => {
            this.deleteMoto(this.moto.id);
            this.router.navigate(['/home']);
          }
        }
      ]
    });
    await alert.present();
  }

  async deleteMoto(id:number){
    const info=await fetch(`http://adrian-val-7e3.alwaysdata.net/miapi/getmotos/${id}`,{
      method:"DELETE"
    })
    return info
  }
}