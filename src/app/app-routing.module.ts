import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'motos',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'detalle-moto',
    loadChildren: () => import('./detalle-motos/detalle-motos.module').then( m => m.DetallePageModule)
  },
  {
    path: 'añadir-moto',
    loadChildren: () => import('./añadir-moto/añadir-moto.module').then( m => m.AñadirMotoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
