import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Moto } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-motos',
  templateUrl: './motos.page.html',
  styleUrls: ['./motos.page.scss'],
})
export class HomePage implements OnInit {

  sitios=[
    {
      title:'Ducati',
      foto:"https://seeklogo.com/images/D/ducati-logo-256EB5E75F-seeklogo.com.png"
    },
    {
      title:'Yamaha',
      foto:"https://e7.pngegg.com/pngimages/875/92/png-clipart-yamaha-motor-company-yamaha-corporation-motorcycle-logo-motorcycle-company-logo.png"
    },
    {
      title:'Honda',
      foto:"https://c0.klipartz.com/pngpicture/103/133/gratis-png-honda-logo-scooter-coche-moto-honda.png"
    },
    {
      title:'Todas las motos',
      foto:"https://www.pngfind.com/pngs/m/365-3650578_motorcycle-motorbike-bumper-dibujos-de-motos-ninjas-hd.png"
    }
  ]
  motos:any
  moto:Moto
  filtrar:string
  
  constructor(private data: DataService ,private router:Router) { 
  }

  ngOnInit() {
    (async ()=>{
      await this.getMotos()
    })()
  }

  async getMotos(){
    const info=await (await fetch(`http://adrian-val-7e3.alwaysdata.net/miapi/getmotos`)).json()
    return info
  }

  ionViewDidEnter(){
    (async ()=>{
      this.motos=await this.getMotos()
    })()
  }

  async filtrarMarcas(marca:string){
    const info=await (await fetch(`http://adrian-val-7e3.alwaysdata.net/miapi/getmotos/filtrar?marca=${marca}`)).json() 
    return info
  }

  filtrarM(marca){
    (async () => {
      if(marca!='Todas'){
        this.motos= await this.filtrarMarcas(marca)
      } 
      else this.motos= await this.getMotos()
    })()
  }

  detalle(moto){
    this.data.setMoto(moto)
    this.router.navigate(['detalle-moto'])
  }

  add(){
    this.router.navigate(['añadir-moto'])
  }  
}