const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???

var multer = require('multer');


const app = express();
const baseUrl = '/miapi';

app.use(cors());


//La configuració de la meva bbdd
ddbbConfig = {
   user: 'adrian-val-7e3_1',
   host: 'postgresql-adrian-val-7e3.alwaysdata.net',
   database: 'adrian-val-7e3_pf',
   password: 'adri1234',
   port: 5432
};

//El pool es un congunt de conexions
//El pool es un congunt de conexions
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
const getMotos = (request, response) => {
   var consulta = "SELECT * FROM  motos"
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
app.get(baseUrl + '/getmotos', getMotos);

const getMotosFiltered = (request, response) => {
   var marca = request.query.marca;
   console.log(request.query.marca);
   var consulta = `SELECT * FROM  motos WHERE marca = '${marca}'`
   pool.query(consulta, (error, results) => {
      if (error) {
          throw error
      }
      response.status(200).json(results.rows)
      console.table(results.rows);
   });
}
app.get(baseUrl + '/getmotos/filtrar', getMotosFiltered);

const deleteMotos = (request, response) => {
   var consulta = `DELETE FROM motos WHERE id=${request.params.id}`
   pool.query(consulta, (error, results) => {
      if (error) {
          throw error
      }
      response.send("Eliminada")
      console.log("Moto eliminada")
   });
}
app.delete(baseUrl + '/getmotos/:id', deleteMotos);

//aqui un exemple de post que pasarem per body
const addMoto = (request, response) => {
   console.log(request.body);
   //const { marca, modelo, year, foto, precio } = request.body
   //console.log(marca, modelo, year, foto, precio);
   response.send('Hola aixo es el que m\'arriva' + JSON.stringify(request.body))
}
app.post(baseUrl + '/moto', addMoto);


//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});
